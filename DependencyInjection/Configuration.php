<?php

namespace Beelab\CatalogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('beelab_catalog');

        $rootNode
            ->children()
                ->scalarNode('product_class')
                    ->isRequired()
                ->end()
                ->scalarNode('product_form')
                    ->cannotBeEmpty()
                    ->defaultValue('Beelab\CatalogBundle\Form\Type\ProductType')
                ->end()
                ->scalarNode('product_filter')
                    ->cannotBeEmpty()
                    ->defaultValue('Beelab\CatalogBundle\Form\Type\ProductFilterType')
                ->end()
                ->scalarNode('product_manager')
                    ->cannotBeEmpty()
                    ->defaultValue('Beelab\CatalogBundle\Manager\ProductManager')
                ->end()
                ->scalarNode('layout')
                    ->cannotBeEmpty()
                    ->defaultValue('BeelabCatalogBundle::layout.html.twig')
                ->end()
                ->scalarNode('theme')
                    ->cannotBeEmpty()
                    ->defaultValue('BeelabCatalogBundle::theme.html.twig')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
