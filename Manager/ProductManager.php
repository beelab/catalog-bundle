<?php

namespace Beelab\CatalogBundle\Manager;

use Beelab\CatalogBundle\Entity\ProductInterface;
use Beelab\CatalogBundle\Event\ProductEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductManager
{
    protected $entityClass, $em, $dispatcher;

    /**
     * @param string                   $entityClass
     * @param EntityManager            $em
     * @param SecurityContextInterface $context
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct($entityClass, EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->entityClass = $entityClass;
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Get QueryBuilder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->em->getRepository($this->entityClass)->createQueryBuilder('p');
    }

    /**
     * Create a new instance of a Product entity
     *
     * @return ProductInterface
     */
    public function getInstance()
    {
        return new $this->entityClass;
    }

    /**
     * Find a Product
     *
     * @param  integer          $id
     * @return ProductInterface
     */
    public function find($id)
    {
        $product = $this->em->getRepository($this->entityClass)->find($id);
        if (empty($product)) {
            throw new NotFoundHttpException('Product not found');
        }

        return $product;
    }

    /**
     * Create a new Product
     *
     * @param ProductInterface $product
     * @param boolean          $flush
     */
    public function create(ProductInterface $product, $flush = true)
    {
        $this->em->persist($product);
        if ($flush) {
            $this->em->flush();
        }
        $this->dispatcher->dispatch('beelab_catalog.product.create', new ProductEvent($product));
    }

    /**
     * Update a Product
     *
     * @param ProductInterface $product
     * @param boolean          $flush
     */
    public function update(ProductInterface $product, $flush = true)
    {
        if ($flush) {
            $this->em->flush();
        }
        $this->dispatcher->dispatch('beelab_catalog.product.update', new ProductEvent($product));
    }

    /**
     * Delete a Product
     *
     * @param ProductInterface $product
     * @param boolean          $flush
     */
    public function delete(ProductInterface $product, $flush = true)
    {
        $this->dispatcher->dispatch('beelab_catalog.product.delete', new ProductEvent($product));
        $this->em->remove($product);
        if ($flush) {
            $this->em->flush();
        }
    }
}