<?php

namespace Beelab\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Product controller.
 *
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * Lists all Product entities.
     *
     * @Route("", name="product")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm($this->get('beelab_catalog.product.filter'));
        if (!is_null($response = $this->saveFilter($form, 'product', 'product'))) {
            return $response;
        }
        $qb = $this->get('beelab_catalog.product.manager')->getQueryBuilder();
        $paginator = $this->filter($form, $qb, 'product');

        return array(
            'form'      => $form->createView(),
            'paginator' => $paginator,
        );
    }

    /**
     * Finds and displays a Product entity.
     *
     * @Route("/{id}/show", name="product_show")
     * @Template()
     */
    public function showAction($id)
    {
        $product = $this->get('beelab_catalog.product.manager')->find($id);
        $deleteForm = $this->createDeleteForm($product->getId());

        return array(
            'product'     => $product,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Product entity.
     *
     * @Route("/new", name="product_new")
     * @Template()
     */
    public function newAction()
    {
        $product = $this->get('beelab_catalog.product.manager')->getInstance();
        $form = $this->createForm($this->get('beelab_catalog.product.form'), $product);

        return array(
            'product' => $product,
            'form'    => $form->createView(),
        );
    }
    /**
     * Creates a new Product entity.
     *
     * @Route("/create", name="product_create")
     * @Method("POST")
     * @Template("BeelabCatalogBundle:Product:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $product = $this->get('beelab_catalog.product.manager')->getInstance();
        $form = $this->createForm($this->get('beelab_catalog.product.form'), $product);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('beelab_catalog.product.manager')->create($product);

            return $this->redirect($this->generateUrl('product_show', array('id' => $product->getId())));
        }

        return array(
            'product' => $product,
            'form'    => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", name="product_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $product = $this->get('beelab_catalog.product.manager')->find($id);
        $editForm = $this->createForm($this->get('beelab_catalog.product.form'), $product);
        $deleteForm = $this->createDeleteForm($product->getId());

        return array(
            'product'     => $product,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Edits an existing Product entity.
     *
     * @Route("/{id}/update", name="product_update")
     * @Method("POST")
     * @Template("BeelabCatalogBundle:Product:edit.html.twig")
     */
    public function updateAction($id, Request $request)
    {
        $product = $this->get('beelab_catalog.product.manager')->find($id);
        $editForm = $this->createForm($this->get('beelab_catalog.product.form'), $product);
        if ($editForm->handleRequest($request)->isValid()) {
            $this->get('beelab_catalog.product.manager')->update($product);

            return $this->redirect($this->generateUrl('product_edit', array('id' => $product->getId())));
        }
        $deleteForm = $this->createDeleteForm($product->getId());

        return array(
            'product'     => $product,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Save order.
     *
     * @Route("/order/{field}/{type}", name="product_sort")
     */
    public function sortAction($field, $type)
    {
        $this->setOrder('product', $field, $type);

        return $this->redirect($this->generateUrl('product'));
    }

    /**
     * Deletes a Product entity.
     *
     * @Route("/{id}/delete", name="product_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id, Request $request)
    {
        $product = $this->get('beelab_catalog.product.manager')->find($id);
        $form = $this->createDeleteForm($product->getId());
        if ($form->handleRequest($request)->isValid()) {
            $this->get('beelab_catalog.product.manager')->delete($product);
        }

        return $this->redirect($this->generateUrl('product'));
    }
}
