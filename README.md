BeelabCatalogBundle
===================

Installation
------------

Add to ``composer.json``

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:beelab/catalog-bundle.git"
    }
],
```

Then ``composer require beelab/catalog-bundle:0.1.*``

Enable bundle in ``app/AppKernel.php``

Configuration
-------------

### Product

Create a ``Product`` entity that extends ``Beelab\CatalogBundle\Entity\Product``.
Add to your ``app/config/config.yml``:
```
beelab_catalog:
    product_class:  Beelab\AcmeBundle\Entity\Product
```

Add to ``app/config/routing.yml``
```
beelab_catalog_product:
    resource: "@BeelabCatalogBundle/Controller/ProductController.php"
    type:     annotation
    prefix:   /
```

### Layout

By default, backend uses a layout called "BeelabCatalog::layout.html.twig" and a form theme
called "BeelabCatalog::theme.html.twig". You can customize these values in
your ``app/config/config.yml``:
```
beelab_catalog:
    # ...
    layout: "::layout.html.twig"
    theme:  "::theme.html.twig"
```
