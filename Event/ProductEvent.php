<?php

namespace Beelab\CatalogBundle\Event;

use Beelab\CatalogBundle\Entity\ProductInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * ProductEvent
 */
class ProductEvent extends Event
{
    protected $product;

    /**
     * @param ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * @return ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }
}