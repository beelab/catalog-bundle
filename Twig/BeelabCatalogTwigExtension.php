<?php

namespace Beelab\CatalogBundle\Twig;

use Twig_Extension;

class BeelabCatalogTwigExtension extends Twig_Extension
{
    protected $layout, $theme;

    /**
     * @param string $layout
     * @param string $theme
     */
    public function __construct($layout, $theme)
    {
        $this->layout = $layout;
        $this->theme = $theme;
    }

    /**
     * {@inheritDoc}
     */
    public function getGlobals()
    {
        return array(
            'beelab_catalog_layout' => $this->layout,
            'beelab_catalog_theme'  => $this->theme,
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'beelab_catalog_twig_extension';
    }

}