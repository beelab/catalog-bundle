<?php

namespace Beelab\CatalogBundle\Tests\Manager;

use Beelab\CatalogBundle\Manager\ProductManager;
use PHPUnit_Framework_TestCase;

/**
 * @group unit
 */
class ProductManagerTest extends PHPUnit_Framework_TestCase
{
    protected
        $manager,
        $em,
        $repository,
        $dispatcher;

    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $this->repository = $this->getMockBuilder('Doctrine\ORM\EntityRepository')->disableOriginalConstructor()->getMock();
        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
        $this->em->expects($this->any())->method('getRepository')->will($this->returnValue($this->repository));

        $this->manager = new ProductManager('pippo', $this->em, $this->dispatcher);
    }

    public function testQueryBuilder()
    {
        $this->markTestIncomplete();
    }

    public function testGetInstance()
    {
        $this->markTestIncomplete();
    }

    public function testCreate()
    {
        $this->markTestIncomplete();
    }

    public function testUpdate()
    {
        $this->markTestIncomplete();
    }

    public function testDelete()
    {
        $this->markTestIncomplete();
    }
}
