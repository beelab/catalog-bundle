<?php

namespace Beelab\CatalogBundle\Tests\Event;

use Beelab\CatalogBundle\Event\ProductEvent;
use PHPUnit_Framework_TestCase;

/**
 * @group unit
 */
class ProductEventTest extends PHPUnit_Framework_TestCase
{
    public function testGetProduct()
    {
        $event = new ProductEvent($this->getMock('Beelab\CatalogBundle\Entity\ProductInterface'));
        $this->assertInstanceOf('Beelab\CatalogBundle\Entity\ProductInterface', $event->getProduct());
    }
}
